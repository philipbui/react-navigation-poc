import { createStore } from 'redux';
import { default as reducers } from './';

export const store = createStore(reducers);