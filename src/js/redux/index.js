import { combineReducers } from 'redux';
import navigation from './modules/navigation';

export default combineReducers({
	navigation,
});
