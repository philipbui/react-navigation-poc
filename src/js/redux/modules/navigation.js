import { createAction, handleActions } from 'redux-actions';
import { Record, List } from 'immutable';

const NAVIGATION = 'app/navigation/';
const PUSH = NAVIGATION + 'PUSH';
const POP = NAVIGATION + 'POP';
const PRESENT = NAVIGATION + 'PRESENT';
const DISMISS = NAVIGATION + 'DISMISS';
const APPLICATION = NAVIGATION + 'APPLICATION';

const NavigationRecord = new Record({
	id: '',
	props: {},
});

export const push = createAction(PUSH, (id, props) => new NavigationRecord({id: id, props: props}));
export const pop = createAction(POP);
export const present = createAction(PRESENT, (id, props) => new NavigationRecord({id: id, props: props}));
export const dismiss = createAction(DISMISS);
export const application = createAction(APPLICATION, (id, props) => new NavigationRecord({id: id, props: props}));

export const actions = {
	push,
	pop,
	present,
	dismiss,
	application
};

export const reducers = {
	[PUSH]: (state, { payload }) => 
		updateLastList(state, list => 
			list.push(payload)
		),
	[POP]: (state) => 
		updateLastList(state, list => 
			list.pop()
		),
	[PRESENT]: (state, { payload }) => 
		updateLastList(state, list => 
			list.push(List([payload]))
		),
	[DISMISS]: (state) => 
		updateSecondLastList(state, list => 
			list.pop()
		),
	[APPLICATION]: (state, { payload }) => 
		List([payload])
	,
};

/**
 * 
 * @param list
 * @param updater (List) => List
 * @returns {*}
 */
export const updateSecondLastList = (list, updater) => {
	let last = list.get(-1);
	if (List.isList(last) && List.isList(last.get(-1))) { //FIXME: Would you optimize this
		return list.update(list => {
			return list.set(list.size - 1, updateSecondLastList(last, updater));
		})
	} else {
		return updater(list);
	}
};

/**
 * 
 * @param list 
 * @param updater (List) => List
 * @returns {*}
 */
export const updateLastList = (list, updater) => {
	let last = list.get(-1);
	if (List.isList(last)) {
		return list.update(list => {
			return list.set(list.size - 1, updateLastList(last, updater));
		})
	} else {
		return updater(list);
	}
};

export const initialState = () => List([]);

export default handleActions(reducers, initialState());