import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { List } from 'immutable';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import { navigation } from '../../redux/selectors/navigation';
import './NavigationViewController.css';
import ViewController from './ViewController';
const mapStateToProps = state => ({
	navigation: navigation(state),
});

const mapDispatchToProps = ({});

class NavigationViewController extends PureComponent {
	
	renderUser(item) {
		const { name, location, email, phone, picture } = item.props;
		const { title, first, last } = name;
		const { street, city, state, postcode } = location;
		return <div>
			<img src={picture.thumbnail}/>
			<h1> { [title, first, last].join(' ') }</h1>
			<h2> { [street, city, state, postcode].join('\n') }</h2>
			<h3> { email }</h3>
			<h3> { phone }</h3>
		</div>
	}
		
	renderScreens(list) {
		return list.map((item, k) => {
			if (List.isList(item)) {
				return <CSSTransition classNames="transition" key={k} timeout={200}>
						<NavigationViewController {...this.props} navigation={item}/>
					</CSSTransition>
			} else {
				return <CSSTransition classNames="transition" key={item.id + k} timeout={200}>
						<ViewController>
							{
								this.renderUser(item)
							}
						</ViewController>
					</CSSTransition>
			}
		});
	}
	
	render() {
		return <TransitionGroup className="modal">
			{
				this.renderScreens(this.props.navigation)
			}
		</TransitionGroup>
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationViewController);