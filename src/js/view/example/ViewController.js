import React, { PureComponent } from 'react';

export default class ViewController extends PureComponent {
	
	constructor(props) {
		super(props);
		this.state = {
			background: this.getRandomColor()
		}
	}
	
	//https://stackoverflow.com/questions/18820733/getting-a-random-background-color-in-javascript
	getRandomColor() {
		const hex = Math.floor(Math.random() * 0xFFFFFF);
		return "#" + ("000000" + hex.toString(16)).substr(-6);
	}
	
	render() {
		return <div className="screen" style={{background: this.state.background}}>
			{ this.props.children }
		</div>
	}
}

