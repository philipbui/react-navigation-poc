import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { actions as navigationActions } from '../../redux/modules/navigation';
import NavigationViewController from './NavigationViewController'

const mapStateToProps = state => ({});

const mapDispatchToProps = {
	...navigationActions,
};

class Example extends PureComponent {
	
	componentDidMount() {
		this.randomUserGenerator()
	}
	
	randomUserGenerator() {
		fetch('https://randomuser.me/api/')
			.then(response => response.json())
			.then(data => {
				this.setState(data.results[0]);
			});
	}
	
	dispatchData(dispatch) {
		dispatch("UserViewController", this.state);
		this.randomUserGenerator();
	}
	
	render() {
		const { push, pop, present, dismiss } = this.props;
		return <div className="modal">
			<button onClick={() => this.dispatchData(push)}>Push</button>
			<button onClick={pop}>Pop</button>
			<button onClick={() => this.dispatchData(present)}>Present</button>
			<button onClick={dismiss}>Dismiss</button>
			<NavigationViewController/>
		</div>;
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Example);

