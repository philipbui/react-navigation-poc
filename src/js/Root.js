import React, { PureComponent } from 'react';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import Example from './view/example/Example';

export default class Root extends PureComponent {
	
	render() {
		return (
			<Provider store={store}>
				<Example/>
			</Provider>
		);
	}
}

