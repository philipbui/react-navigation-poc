import React, { Component } from 'react';
import Root from './js/Root'

class App extends Component {
  render() {
    return (
      <Root />
    );
  }
}

export default App;
