## React Navigation PoC

The concept is to have a*Navigation Component*responsible for animating the pushing, popping, presenting and dismissing (modal) of it's children given a certain*Navigation Stack*
 
This example shows a simple navigation stack in Redux (You can use State or Redux State), with a Navigation Component that renders a random user from[Random User Generator](https://randomuser.me/)for every pushed and presented component. **Push, Pop and Modal Animations**

The navigation stack rendering shown is not ideal, you can use a global memoized function that returns a components given id / args, or use a HoC or Container to inject children directly. (Don't inject functions, they will fail the shallow equality test).